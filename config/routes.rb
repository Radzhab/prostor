Rails.application.routes.draw do
  root  'application_staffs#index'
  resources :application_staffs, path: 'staffs'
  resources :hirers
  resources :settings
  resources :regions
  resources :settlements

  match '/getStaff',  to: 'application_staffs#getStaff', via: [:get,:post]
  match '/getSettlements',  to: 'settlements#getSettlements', via: [:get,:post]

  resources :positions, path: 'positions'
  resources :managers
  resources :spheres
  resources :nationalities
  resources :stop_sheet_statuses
end
