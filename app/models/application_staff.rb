class ApplicationStaff < ApplicationRecord
  belongs_to :nationality, optional: true
  belongs_to :emp_type, optional: true
  belongs_to :etnicity, optional: true
  belongs_to :status_interview, optional: true
  belongs_to :status_treatment, optional: true
  belongs_to :stop_sheet_status, optional: true
  belongs_to :residence_document, optional: true
  belongs_to :manager, optional: true
  belongs_to :desired_region, class_name: Region
  belongs_to :patent_region, class_name: Region,optional: true

  has_many :app_staff_pos_positions
  has_many :possible_positions, through: :app_staff_pos_positions
  has_many :settlements

  enum sex: { man: 0, woman: 1 }


  mount_uploader :avatar, AvatarUploader

  #validates_presence_of :surname, :name, :patronymic, :message => "Заполните обязательные поля"

  def formatted_name
    "#{surname}  #{name} #{patronymic}"
  end
end
