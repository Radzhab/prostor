class PossiblePosition < ApplicationRecord
  has_many :app_staff_pos_positions
  has_many :application_staffs, through: :app_staff_pos_positions
end