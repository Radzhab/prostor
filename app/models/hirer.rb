class Hirer < ApplicationRecord

  belongs_to :sphere, optional: true
  has_many :settlements

end
