class Settlement < ApplicationRecord
  include EnumHelper
  enum payment_type: { cash: 0, cashless: 1 }

  belongs_to :hirer, optional: true
  belongs_to :position, optional: true
  belongs_to :application_staff, optional: true

  def formatted_shift
    shift.strftime("%H:%M") if shift?
  end


end