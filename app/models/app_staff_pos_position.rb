class AppStaffPosPosition < ApplicationRecord
  belongs_to :application_staff, optional: true
  belongs_to :possible_position, optional: true
end
