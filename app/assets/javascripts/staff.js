
var columnDefs = [

    {
        headerName: 'База книг',
        children: [
            {
                headerName: "Фамилия", field: "surname",
                width: 180, pinned: true
            },
            {
                headerName: "Имя", field: "name",
                width: 180, pinned: true
            },
            {
                headerName: "Телефон", field: "phone1",
                width: 180, pinned: true
            },
            {
                headerName: "Возраст", field: "age",
                filter: 'number',
                width: 80, pinned: true
            },
            {
                headerName: "Статус трудоустройства", field: "status_treatment.display_name",
                width: 180, pinned: true
            },
            {
                headerName: "Статус интервью", field: "status_interview.display_name",
                width: 180, pinned: true
            },
            {
                headerName: "Тип трудоустройства", field: "emp_type.display_name",
                width: 180, pinned: true
            },
            {
                headerName: "Дата обращения", field: "treatment_date",
                filter: 'date',
                width: 150, pinned: true,
                valueGetter: function (params) {
                        var input = params.data.treatment_date;

                        try {
                                var r = /\d+/g;
                                var datePart = (input.match(r));

                                var year = datePart[0];// get only two digits
                                var month = datePart[1];
                                var day = datePart[2];
                                return day+'.'+month+'.'+year;

                            } catch (err) {
                            
                                // return '1899-30-12'
                            }
                        

                    }
            },
            {
                headerName: "Гражданство", field: "nationality.display_name",
                width: 180, pinned: true
            },
            {
                headerName: "Желаемая должность", field: "possible_positions",
                width: 180, pinned: true,
                valueGetter: function (params) {
                    var x =params.data.possible_positions.map(elem => elem.display_name).join(";");
                    return x;
                }
            },

            {
                headerName: "Изменить", field: "id",
                width: 180, pinned: true,
                cellRenderer: function (params) {
                    console.log(params)
                    return '<a  href="/staffs/'+params.value+'/edit">Изменить</a>'
                }

            }

        ]
    }
];

// let the grid know which columns and what data to use
var gridOptions = {
    columnDefs: columnDefs,
    rowData: null,
    enableSorting: true,
    enableFilter: true,
    enableColResize: true,
    floatingFilter:true



};




var axi
document.addEventListener('DOMContentLoaded', function()
{
    if (!axi) {
        var gridDiv = document.querySelector('#myGrid');
        axi = new agGrid.Grid(gridDiv, gridOptions);
    }
        gridOptions.api.sizeColumnsToFit();

        var httpRequest = new XMLHttpRequest();
        httpRequest.open('GET', 'getStaff');
        httpRequest.send();
        httpRequest.onreadystatechange = function () {
            if (httpRequest.readyState == 4 && httpRequest.status == 200) {
                var httpResult = JSON.parse(httpRequest.responseText);
                gridOptions.api.setRowData(httpResult);
            }
        };
});