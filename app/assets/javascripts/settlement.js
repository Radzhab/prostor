
var columnDefs = [

    {
        headerName: 'База книг',
        children: [
            {
                headerName: "Дата", field: "date",
                width: 180, pinned: true,
                valueGetter: function (params) {
                    var input = params.data.date;

                    try {
                        var r = /\d+/g;
                        var datePart = (input.match(r));

                        var year = datePart[0];// get only two digits
                        var month = datePart[1];
                        var day = datePart[2];
                        return day+'.'+month+'.'+year;

                    } catch (err) {

                        // return '1899-30-12'
                    }


                }
            },
            {
                headerName: "ID объекта", field: "hirer.display_name",
                width: 350, pinned: true
            },
            {
                headerName: "Позиция", field: "position.display_name",
                width: 350, pinned: true
            },
            {
                headerName: "IDE", field: "application_staff.id",
                width: 150, pinned: true
            },
            {
                headerName: "Фамилия", field: "application_staff.surname",
                width: 180, pinned: true
            },
            {
                headerName: "Имя", field: "application_staff.name",
                width: 180, pinned: true
            },
            {
                headerName: "Контактный телефон", field: "application_staff.phone1",
                width: 180, pinned: true
            },
            {
                headerName: "Начало рабочей смены", field: "shift",
                width: 180, pinned: true
            },
            {
                headerName: "Фактическое время прибытия", field: "actual_arrival_time",
                width: 180, pinned: true
            },
            {
                headerName: "Обед", field: "lunch",
                width: 180, pinned: true
            },
            {
                headerName: "Фактическое время ухода", field: "time_care",
                width: 180, pinned: true
            },
            {
                headerName: "Расчетное время", field: "time_care",
                width: 180, pinned: true
            },
            {
                headerName: "Ставка", field: "rate",
                width: 180, pinned: true
            },
            {
                headerName: "Сумма к выплате", field: "amount",
                width: 180, pinned: true
            },
            {
                headerName: "Выплачено", field: "paid",
                width: 180, pinned: true
            },
            {
                headerName: "Задолженность", field: "debt",
                width: 180, pinned: true
            },
            {
                headerName: "Выплачено", field: "paid",
                width: 180, pinned: true
            },
            {
                headerName: "Тип оплаты", field: "payment_type",
                width: 180, pinned: true
            },
            {
                headerName: "Выход на работу", field: "going_to_work",
                width: 180, pinned: true
            },
            {
                headerName: "Коментарии", field: "comments",
                width: 180, pinned: true
            },
            {
                headerName: "Стоп-лист", field: "application_staff.stop_sheet",
                width: 180, pinned: true,
                cellRendererParams: {
                    suppressCount: true,
                    checkbox: true,
                    padding: 20

                }
            },
            {
                headerName: "Гражданство", field: "nationality.display_name",
                width: 180, pinned: true
            },
            {
                headerName: "Изменить", field: "id",
                width: 180, pinned: true,
                cellRenderer: function (params) {
                    console.log(params)
                    return '<a  href="/settlement/'+params.value+'/edit">Изменить</a>'
                }

            }

        ]
    }
];

// let the grid know which columns and what data to use
var gridOptions = {
    columnDefs: columnDefs,
    rowData: null,
    enableSorting: true,
    enableFilter: true,
    enableColResize: true,
    floatingFilter:true



};




var axi
document.addEventListener('DOMContentLoaded', function()
{
    if (!axi) {
        var gridDiv = document.querySelector('#ag_settlements');
        axi = new agGrid.Grid(gridDiv, gridOptions);
    }
        gridOptions.api.sizeColumnsToFit();

        var httpRequest = new XMLHttpRequest();
        httpRequest.open('GET', 'getSettlements');
        httpRequest.send();
        httpRequest.onreadystatechange = function () {
            if (httpRequest.readyState == 4 && httpRequest.status == 200) {
                var httpResult = JSON.parse(httpRequest.responseText);
                gridOptions.api.setRowData(httpResult);
            }
        };
});