class PossiblePositionsController < ApplicationController
  before_action :set_possible_position, only: [:show, :edit, :update, :destroy]

  # GET /possible_positions
  def index
    @possible_positions = PossiblePosition.all
  end

  # GET /possible_positions/1
  def show
  end

  # GET /possible_positions/new
  def new
    @possible_position = PossiblePosition.new
  end

  # GET /possible_positions/1/edit
  def edit
  end

  # POST /possible_positions
  def create
    @possible_position = PossiblePosition.new(possible_position_params)

    if @possible_position.save
      redirect_to @possible_position, notice: 'Possible position was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /possible_positions/1
  def update
    if @possible_position.update(possible_position_params)
      redirect_to @possible_position, notice: 'Possible position was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /possible_positions/1
  def destroy
    @possible_position.destroy
    redirect_to possible_positions_url, notice: 'Possible position was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_possible_position
      @possible_position = PossiblePosition.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def possible_position_params
      params.fetch(:possible_position, {})
    end
end
