class SettlementsController < ApplicationController
  before_action :set_settlement, only: [:show, :edit, :update, :destroy]

  def getSettlements
    @tmp = Settlement.includes(:hirer, :position, :application_staff)
    render json: @tmp.as_json(include: [:hirer,:position, :application_staff]);

  end


  # GET /settlements
  def index
    @settlements = Settlement.all
  end

  # GET /settlements/1
  def show
  end

  # GET /settlements/new
  def new
    @settlement = Settlement.new
  end

  # GET /settlements/1/edit
  def edit
    @settlement = Settlement.find(params[:id])
  end

  # POST /settlements
  def create
    @settlement = Settlement.new(settlement_params)

    if @settlement.save
      redirect_to settlement_url, notice: 'Settlement was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /settlements/1
  def update
    if @settlement.update(settlement_params)
      redirect_to settlement_url, notice: 'Settlement was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /settlements/1
  def destroy
    @settlement.destroy
    redirect_to settlements_url, notice: 'Settlement was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_settlement
      @settlement = Settlement.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def settlement_params
      #validation fields
      t1 = DateTime.parse("1899-12-30")
      t2 = (DateTime.parse(params[:settlement][:shift]) rescue nil)
      t3 = (DateTime.parse(params[:settlement][:actual_arrival_time]) rescue nil)
      t4 = (DateTime.parse(params[:settlement][:lunch]) rescue nil)
      t5 = (DateTime.parse(params[:settlement][:time_care]) rescue nil)

      if (params[:settlement][:shift]=='')
        params[:settlement][:shift]=DateTime.strptime('1899-12-30 00-00','%Y-%m-%d %H-%M')
        t2 = params[:settlement][:shift]
      end

      if (params[:settlement][:actual_arrival_time]=='')
        params[:settlement][:actual_arrival_time]=DateTime.strptime('1899-12-30 00-00','%Y-%m-%d %H-%M')
        t3 = params[:settlement][:actual_arrival_time]
      end

      if (params[:settlement][:lunch]=='')
        params[:settlement][:lunch]=DateTime.strptime('1899-12-30 00-00','%Y-%m-%d %H-%M')
        t4 = params[:settlement][:lunch]
      end

      if (params[:settlement][:time_care]=='')
        params[:settlement][:time_care]=DateTime.strptime('1899-12-30 00-00','%Y-%m-%d %H-%M')
        t5 = params[:settlement][:time_care]
      end

      params[:settlement][:shift]=DateTime.new(t1.year, t1.month, t1.day, t2.hour, t2.min)
      params[:settlement][:actual_arrival_time]=DateTime.new(t1.year, t1.month, t1.day, t3.hour, t3.min)
      params[:settlement][:lunch]=DateTime.new(t1.year, t1.month, t1.day, t4.hour, t4.min)
      params[:settlement][:time_care]=DateTime.new(t1.year, t1.month, t1.day, t5.hour, t5.min)

      require 'time'

      time_care = Time.parse(params[:settlement][:time_care].to_s)
      actual_arrival_time = Time.parse(params[:settlement][:actual_arrival_time].to_s);
      lunch = Time.parse(params[:settlement][:lunch].to_s);
      if (time_care.to_s=="00:00")
        time_care = Time.parse("24:00").strftime("%H:%M");
      end

      t1 = time_care - actual_arrival_time
      t1 = DateTime.parse(Time.at(t1).utc.strftime("%H:%M"))
      t2 = lunch.to_datetime;
      rez = Time.parse(t1.to_s)-Time.parse(t2.to_s);
      axi = Time.at(rez).utc.strftime("%H-%M")
      tmp = DateTime.strptime('1899-12-30 '+axi,'%Y-%m-%d %H-%M')
      params[:settlement][:actual_hours_worked]= tmp

      #Вычисление ставки и прочее
      rate =params[:settlement][:rate].to_f;
      hour= tmp.hour.to_f;

      params[:settlement][:amount]= rate*hour;
      amount  = params[:settlement][:amount].to_f;
      paid  = params[:settlement][:paid].to_f;
      params[:settlement][:debt] =amount-paid;


      @settleparams ||=
          params.require(:settlement).
              permit(
                  :hirer_id,
                  :position_id,
                  :application_staff_id,
                  :date,
                  :shift, # Начало рабочей смены
                  :actual_arrival_time,# Фактическое время прибытия
                  :lunch,  # Обед
                  :time_care, # Фактическое время ухода
                  :actual_hours_worked, # Фактически отработанное время
                  :rate, # Ставка
                  :amount, # Сумма к выплате
                  :paid,   # Выплачено
                  :debt,   # Задолженность по ЗП
                  :payment_type,
                  :comments,
                  :going_to_work)
      return @settleparams
    end
  end

