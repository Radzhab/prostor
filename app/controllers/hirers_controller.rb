
class HirersController < ApplicationController
  before_action :set_hirer, only: [:show, :edit, :update, :destroy]

  # GET /hirers
  def index

    @hirers = Hirer.all
  end

  # GET /hirers/1
  def show
  end

  # GET /hirers/new
  def new
    @hirer = Hirer.new
  end

  # GET /hirers/1/edit
  def edit
    @hirer = Hirer.find(params[:id])
  end

  # POST /hirers
  def create
    @hirer = Hirer.new(hirer_params)

    if @hirer.save
      redirect_to hirer_url, notice: 'Hirer was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /hirers/1
  def update
    if @hirer.update(hirer_params)
      redirect_to hirers_url, notice: 'Hirer was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /hirers/1
  def destroy
    @hirer.destroy
    redirect_to hirers_url, notice: 'Hirer was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hirer
      @hirer = Hirer.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def hirer_params
      params.require(:hirer).permit(
          :display_name,
          :inn,
          :address,
          :ceo,
          :org_phone,
          :comment,
          :contact_person1,
          :phone1,
          :contact_person2,
          :phone2,
          :history,
          :sphere_id
      )
    end
end
