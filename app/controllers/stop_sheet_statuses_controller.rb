class StopSheetStatusesController < ApplicationController
  before_action :set_stop_sheet_status, only: [:show, :edit, :update, :destroy]

  # GET /stop_sheet_statuses
  def index
    @stop_sheet_statuses = StopSheetStatus.all
  end

  # GET /stop_sheet_statuses/1
  def show
  end

  # GET /stop_sheet_statuses/new
  def new
    @stop_sheet_status = StopSheetStatus.new
  end

  # GET /stop_sheet_statuses/1/edit
  def edit
    @stop_sheet_status = StopSheetStatus.find(params[:id])
  end

  # POST /stop_sheet_statuses
  def create
    @stop_sheet_status = StopSheetStatus.new(stop_sheet_status_params)

    if @stop_sheet_status.save
      redirect_to @stop_sheet_status, notice: 'Stop sheet status was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /stop_sheet_statuses/1
  def update
    if @stop_sheet_status.update(stop_sheet_status_params)
      redirect_to stop_sheet_statuses_url, notice: 'Stop sheet status was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /stop_sheet_statuses/1
  def destroy
    @stop_sheet_status.destroy
    redirect_to stop_sheet_statuses_url, notice: 'Stop sheet status was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stop_sheet_status
      @stop_sheet_status = StopSheetStatus.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def stop_sheet_status_params
      params.require(:stop_sheet_status).permit(:id, :display_name)
    end
end
