class ApplicationStaffsController < ApplicationController
  before_action :set_application_staff, only: [:show, :edit, :update, :destroy]

  def getStaff
    @tmp = ApplicationStaff.includes(:status_treatment, :status_interview, :emp_type,
                                     :emp_type, :nationality, :possible_positions)
    render json: @tmp.as_json(include: [:status_treatment,:status_interview,
                                         :nationality,:emp_type, :possible_positions]);

  end

  # GET /application_staffs
  def index
      @application_staffs = ApplicationStaff.all
  end

  # GET /application_staffs/1
  def show
  end

  # GET /application_staffs/new
  def new
    @possible_position = PossiblePosition.all
    @application_staff = ApplicationStaff.new
  end

  # GET /application_staffs/1/edit
  def edit
    @possible_position = PossiblePosition.all
    @application_staff = ApplicationStaff.find(params[:id])
  end

  # POST /application_staffs
  def create
    @application_staff = ApplicationStaff.new(application_staff_params)

    if @application_staff.save
      flash[:success]="Пользователь успешно создан"
      redirect_to application_staffs_url
    else
      flash[:alert]="Возникли проблемы"
      render :new
    end
  end

  # PATCH/PUT /application_staffs/1
  def update
    if @application_staff.update(application_staff_params)
      redirect_to application_staffs_path, notice: 'Application staff was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /application_staffs/1
  def destroy
    @application_staff.destroy
    redirect_to application_staffs_url, notice: 'Application staff was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_application_staff
      @application_staff = ApplicationStaff.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def application_staff_params
      params.require(:application_staff).
          permit(:id, :avatar,
                      :surname,
                      :name,
                      :patronymic,
                      :birth_date,
                      :etnicity_id,
                      :age,
                      :birth_place,
                      :passport,
                      :passport_issued_by,
                      :date_issue,
                      :validate_period,
                      :nationality_id,
                      :residence_document_id,
                      :migration_card,
                      :tensure,
                      :patent,
                      :registration,
                      :med_book,
                      :patent_region_id,
                      :desired_region_id,
                      :stop_sheet,
                      :stop_sheet_status_id,
                      :emp_type_id,
                      :qualification,
                      :status_treatment_id,
                      :status_interview_id,
                      :treatment_date,
                      :interview_date,
                      :phone1,
                      :comment,
                      :bank_card,
                      :email,
                      :manager_id,
                      possible_position_ids: [])

    end

    def permit!
      each_pair do |key, value|
        convert_hashes_to_parameters(key, value)
        self[key].permit! if self[key].respond_to? :permit!
      end

      @permitted = true
      self
    end
end
