class SpheresController < ApplicationController
  before_action :set_sphere, only: [:show, :edit, :update, :destroy]

  # GET /spheres
  def index
    @spheres = Sphere.all
  end

  # GET /spheres/1
  def show
  end

  # GET /spheres/new
  def new
    @sphere = Sphere.new
  end

  # GET /spheres/1/edit
  def edit
    @sphere = Sphere.find(params[:id])
  end

  # POST /spheres
  def create
    @sphere = Sphere.new(sphere_params)

    if @sphere.save
      redirect_to spheres_url, notice: 'Sphere was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /spheres/1
  def update
    if @sphere.update(sphere_params)
      redirect_to spheres_url, notice: 'Sphere was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /spheres/1
  def destroy
    @sphere.destroy
    redirect_to spheres_url, notice: 'Sphere was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sphere
      @sphere = Sphere.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def sphere_params
      params.require(:sphere).permit(:id, :display_name);
    end
end
