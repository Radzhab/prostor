class CreateStatusTreatments < ActiveRecord::Migration[5.1]
  def change
    create_table :status_treatments do |t|
      t.string :display_name

      t.timestamps
    end
  end
end
