class AddHirerToSettlements < ActiveRecord::Migration[5.1]
  def change
    add_reference :settlements, :hirer, foreign_key: true
  end
end
