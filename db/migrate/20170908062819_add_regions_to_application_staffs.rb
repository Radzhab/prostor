class AddRegionsToApplicationStaffs < ActiveRecord::Migration[5.1]
  def change
    add_column :application_staffs, :desired_region_id, :integer
    add_column :application_staffs, :patent_region_id, :integer
  end
end
