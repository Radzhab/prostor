class CreateManagers < ActiveRecord::Migration[5.1]
  def change
    create_table :managers do |t|
      t.string :display_name
      t.boolean :ouster

      t.timestamps
    end
  end
end
