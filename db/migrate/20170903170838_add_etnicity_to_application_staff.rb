class AddEtnicityToApplicationStaff < ActiveRecord::Migration[5.1]
  def change
    add_reference :application_staffs, :etnicity, foreign_key: true
  end
end
