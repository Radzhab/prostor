class AddManagerToApplicationStaff < ActiveRecord::Migration[5.1]
  def change
    add_reference :application_staffs, :manager, foreign_key: true
  end
end
