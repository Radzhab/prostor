class AddSphereToHirer < ActiveRecord::Migration[5.1]
  def change
    add_reference :hirers, :sphere, foreign_key: true
  end
end
