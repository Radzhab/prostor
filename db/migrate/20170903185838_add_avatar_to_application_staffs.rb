class AddAvatarToApplicationStaffs < ActiveRecord::Migration[5.1]
  def change
    add_column :application_staffs, :avatar, :string
  end
end
