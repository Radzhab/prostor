class CreateStopSheetStatuses < ActiveRecord::Migration[5.1]
  def change
    create_table :stop_sheet_statuses do |t|
      t.string :display_name

      t.timestamps
    end
  end
end
