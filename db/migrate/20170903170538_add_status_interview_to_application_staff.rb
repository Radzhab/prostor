class AddStatusInterviewToApplicationStaff < ActiveRecord::Migration[5.1]
  def change
    add_reference :application_staffs, :status_interview, foreign_key: true
  end
end
