class CreateStatusInterviews < ActiveRecord::Migration[5.1]
  def change
    create_table :status_interviews do |t|
      t.string :display_name

      t.timestamps
    end
  end
end
