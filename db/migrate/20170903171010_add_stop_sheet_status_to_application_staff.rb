class AddStopSheetStatusToApplicationStaff < ActiveRecord::Migration[5.1]
  def change
    add_reference :application_staffs, :stop_sheet_status, foreign_key: true
  end
end
