
class CreateEmpTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :emp_types do |t|
      t.string :display_name

      t.timestamps
    end
  end
end
