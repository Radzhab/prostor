class CreateResidenceDocuments < ActiveRecord::Migration[5.1]
  def change
    create_table :residence_documents do |t|
      t.string :display_name

      t.timestamps
    end
  end
end
