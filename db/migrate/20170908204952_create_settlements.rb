class CreateSettlements < ActiveRecord::Migration[5.1]
  def change
    create_table :settlements do |t|
      t.datetime :date
      t.datetime :shift # Начало рабочей смены
      t.datetime :actual_arrival_time # Фактическое время прибытия
      t.datetime :lunch  # Обед
      t.datetime :time_care # Фактическое время ухода
      t.datetime :actual_hours_worked # Фактически отработанное время
      t.decimal :rate # Ставка
      t.decimal :amount # Сумма к выплате
      t.decimal :paid   # Выплачено
      t.decimal :debt   # Задолженность по ЗП
      t.integer :payment_type
      t.string  :comments
      t.boolean :going_to_work
    end
  end
end
