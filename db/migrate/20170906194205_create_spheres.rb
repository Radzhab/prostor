class CreateSpheres < ActiveRecord::Migration[5.1]
  def change
    create_table :spheres do |t|
      t.string :display_name

      t.timestamps
    end
  end
end
