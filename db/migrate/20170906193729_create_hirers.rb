
class CreateHirers < ActiveRecord::Migration[5.1]
  def change
    create_table :hirers do |t|
      t.string :display_name
      t.string :inn
      t.string :address
      t.string :ceo
      t.string :org_phone
      t.string :comment
      t.string :contact_person1
      t.string :phone1
      t.string :contact_person2
      t.string :phone2
      t.string :history

    end
  end
end
