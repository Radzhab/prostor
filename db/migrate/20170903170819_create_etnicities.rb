class CreateEtnicities < ActiveRecord::Migration[5.1]
  def change
    create_table :etnicities do |t|
      t.string :display_name

      t.timestamps
    end
  end
end
