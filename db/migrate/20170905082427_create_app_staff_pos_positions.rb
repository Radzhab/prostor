class CreateAppStaffPosPositions < ActiveRecord::Migration[5.1]
  def change
    create_table :app_staff_pos_positions do |t|
      t.integer :application_staff_id
      t.integer :possible_position_id

      t.timestamps
    end
  end
end
