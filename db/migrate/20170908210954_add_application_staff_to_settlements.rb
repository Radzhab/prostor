class AddApplicationStaffToSettlements < ActiveRecord::Migration[5.1]
  def change
    add_reference :settlements, :application_staff, foreign_key: true
  end
end
