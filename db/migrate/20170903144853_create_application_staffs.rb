class CreateApplicationStaffs < ActiveRecord::Migration[5.1]
  def change
    create_table :application_staffs do |t|
      t.string :surname      #Фамилия
      t.string      :name         #Имя
      t.string      :patronymic   #Отчество
      t.string      :patronymic   #Отчество
      t.date    :birth_date   #Полная дата рождения
      #t.date        :birth_year   #Год рождения
      t.integer     :age          #Возраст
      t.string      :passport     #Паспорт
      t.string      :birth_place  #Место рождения
      t.string      :passport_issued_by #Кем выдан паспорт
      t.date    :date_issue   #Дата выдачи
      t.date    :validate_period #Срок действия
      t.string      :comment      #Коментарий
      #t.string      :nationality  ##Национальность(Гражданство)- сделано
      #t.string      :residence_doc #Документ пребывания
      t.string      :tensure      #Срок действия
      #t.string      :emp_type     ##Тип трудоустройства -- сделано
      t.string      :qualification #Квалификация
      #t.string      :possible_position ##Возможные позиции
      #t.string      :status_treatment ##Статус обращения --
      #t.string      :status_interview ##Статус собеседования --
      t.date    :treatment_date  #Дата обращения
      t.date    :interview_date  #Дата прохождения собеседования
      t.string      :phone1          #Телефон1
      t.string      :phone2          #Телефон2
      t.boolean        :patent          #Патент
      #t.string      :patent_region   #Регион выдачи патента
      t.boolean        :migration_card  #Миграционная карта
      t.boolean        :registration    #Регистрация
      t.boolean        :med_book        #Медицинская книжка
      #t.string      :desired_region  ##Желаемый регион трудоустройства -- многие ко многим имхо
      t.boolean        :stop_sheet       #Стоп лиcт
      #t.string      :stop_sheet_status ##Статус стоп листа
      t.string      :bank_card        #Банковская карта
      #t.string      :client_manager   #Клиентский менеджер
      t.boolean     :import_record    #Импорт записи
      t.integer      :sex              #Пол
      t.integer     :staff_uniq     #Счет
      #t.string      :etnicity         ## Тип внешности -- сделано
      t.string       :email            #Почта

      t.timestamps
    end
  end
end
