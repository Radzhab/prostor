class AddPositionToSettlement < ActiveRecord::Migration[5.1]
  def change
    add_reference :settlements, :position, foreign_key: true
  end
end
