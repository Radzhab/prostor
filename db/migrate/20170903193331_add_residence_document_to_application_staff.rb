class AddResidenceDocumentToApplicationStaff < ActiveRecord::Migration[5.1]
  def change
    add_reference :application_staffs, :residence_document, foreign_key: true
  end
end
