class AddNationalityToApplicationStaff < ActiveRecord::Migration[5.1]
  def change
    add_reference :application_staffs, :nationality, foreign_key: true
  end
end
