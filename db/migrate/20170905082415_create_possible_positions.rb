class CreatePossiblePositions < ActiveRecord::Migration[5.1]
  def change
    create_table :possible_positions do |t|
      t.string :display_name
      t.timestamps
    end
  end
end
