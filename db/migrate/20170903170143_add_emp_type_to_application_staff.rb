class AddEmpTypeToApplicationStaff < ActiveRecord::Migration[5.1]
  def change
    add_reference :application_staffs, :emp_type, foreign_key: true
  end
end
