require 'date'
require 'time'
def to_boolean(str)
  str == '1'
end

def payment_beautiful(str)
  if str=="наличный"
    return 0;
  else
    return 1;
  end
end

def sex_beautiful(str)
  if str=="Мужской"
    return 0;
  else
    return 1;
  end
end

file  = File.open( File.join(Rails.root,'db/AppStaff.xlsx'))
workbook = Roo::Excelx.new(file)
workbook.default_sheet = workbook.sheets[0]

ind = -1
workbook.each_row_streaming(pad_cells: true) do |row|
  ind = ind+1;
  if (ind!=0)
    if (!row[1].to_s.empty?)
        appstaff = ApplicationStaff.create(
            id: Integer(row[0].to_s),
        surname: row[1].to_s, # Фамилия
        name: row[2].to_s,          # Имя
        patronymic: row[3].to_s,    # Отчество
        birth_date: (DateTime.strptime(row[4].to_s,"%d/%m/%Y") rescue nil),    # Полная дата рождения
        age: row[6].to_s,           # Возраст
        passport: row[7].to_s,      # Паспорт
        birth_place: row[8].to_s,   # Место рождения
        passport_issued_by: row[9].to_s, # Кем выдан паспорт
        date_issue: (DateTime.strptime(row[10].to_s,"%d/%m/%Y") rescue nil),    # Дата выдачи
        validate_period: (DateTime.strptime(row[11].to_s,"%d/%m/%Y")  rescue nil), # Срок действия
        comment: row[12].to_s,      # Коментарий
        nationality_id: Nationality.find_or_create_by(display_name: row[13].to_s)[:id], #Гражданство
        residence_document_id: ResidenceDocument.find_or_create_by(display_name: row[14].to_s)[:id], # Док. пребывания
        tensure: row[15].to_s,      # Срок действия ДП
        emp_type_id: EmpType.find_or_create_by(display_name: row[16].to_s)[:id], # Тип трудоустройства
        qualification: row[17].to_s, # Квалификаци
        status_treatment_id: StatusTreatment.find_or_create_by(display_name: row[19].to_s)[:id], # Статус обращения
        status_interview_id: StatusInterview.find_or_create_by(display_name: row[20].to_s)[:id], # Статус Собеседования
        treatment_date: (DateTime.strptime(row[21].to_s,"%d/%m/%Y") rescue nil),   # Дата обращения
        interview_date: (DateTime.strptime(row[22].to_s,"%d/%m/%Y") rescue nil),   # Дата прохождения собеседования
        phone1: row[23].to_s,   # Контактный телефон 1
        phone2: row[24].to_s,   # Контактный телефон 2
        patent: row[25].to_s,   # Патент
        patent_region_id: Region.find_or_create_by(display_name: row[26].to_s)[:id], # Регион выдачи патента
        migration_card: to_boolean(row[27].to_s),   # Миграционная карта
        registration: to_boolean(row[28].to_s),   # Регистрация
        med_book: to_boolean(row[29].to_s),   # Медкнижка
        desired_region_id: Region.find_or_create_by(display_name: row[30].to_s)[:id],
        stop_sheet: to_boolean(row[31].to_s),   # Стоп-лист
        stop_sheet_status: StopSheetStatus.find_or_create_by(display_name: row[32].to_s), # Стоп-лист статус
        bank_card: row[33].to_s, # Банковская карта,
        manager_id: Manager.find_or_create_by(display_name: row[34].to_s)[:id], # Манагер
        import_record: to_boolean(row[35].to_s),   # Импорт записи
        sex: sex_beautiful(row[36].to_s),   # Пол
        staff_uniq: row[37].to_s,       # ID
        etnicity_id:  Etnicity.find_or_create_by(display_name: row[38].to_s)[:id], # Тип внешности
        email: row[39].to_s
    )

        row[18].to_s.split(';').each do |wor|
          posibpos = PossiblePosition.find_or_create_by(display_name: wor) # Возможные позиции
          posibpos.save!
          appstaff.save!

          ax = AppStaffPosPosition.create!({application_staff_id: appstaff[:id], possible_position_id: posibpos[:id]})
          ax.save!
        end


    end
  end
  end

  puts "грузим hirer"

  file  = File.open( File.join(Rails.root,'db/Hirer.xlsx'))
  workbook = Roo::Excelx.new(file)
  workbook.default_sheet = workbook.sheets[0]

  ind = -1
  workbook.each_row_streaming(pad_cells: true) do |row|
    ind = ind+1;
    if (ind!=0)
      hirer = Hirer.create(
        id: Integer(row[0].to_s),
        display_name: row[1].to_s,
        inn: row[2].to_s,
        address: row[3].to_s,
        ceo: row[4].to_s,
        org_phone: row[5].to_s,
        comment: row[6].to_s,
        sphere_id:  Sphere.find_or_create_by(display_name: row[7].to_s)[:id], # Сфера деятельности
          contact_person1: row[8],
          phone1: row[9],
          contact_person2: row[10],
          phone2: row[11],
          history: row[12]

      )
    end
  end



puts "Грузим settlements"
require 'csv'


csv_text  = File.read( File.join(Rails.root,'db/Settlements.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')

ind = -1
csv.each do |row|
  ind = ind+1;
  if (ind!=0)
    puts row[1].to_s
    settlement= Settlement.create(
        date: (DateTime.parse(row[1].to_s) rescue nil),
        hirer_id: (Hirer.find(row[2].to_s)[:id] rescue nil),
        position_id: Position.find_or_create_by(display_name: row[3].to_s)[:id],

        application_staff_id: (ApplicationStaff.find(row[4].to_s)[:id] rescue nil),
        shift: (DateTime.parse(row[5].to_s) rescue nil), # Начало рабочей смены
        actual_arrival_time: (DateTime.parse(row[6].to_s) rescue nil), # Фактическое время прибытия
        lunch: (DateTime.parse(row[7].to_s) rescue nil), # Обед
        time_care: (DateTime.parse(row[8].to_s) rescue nil), # Фактическое время ухода
        actual_hours_worked: (DateTime.parse(row[9].to_s) rescue nil), # Фактическое отработанное время
        rate: row[10].to_s, # Ставка
        amount: row[11].to_s , # Сумма к выплате
        paid: row[12].to_s , # Выплачено
        debt: row[13].to_s , # Задолженность
        payment_type: payment_beautiful(row[14].to_s),
        comments: row[15].to_s , # Коментарий
        going_to_work: to_boolean(row[16].to_s) , # Выход на работу
    )
    settlement.save!
  end
end