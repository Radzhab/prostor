# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170911075631) do

  create_table "app_staff_pos_positions", force: :cascade do |t|
    t.integer "application_staff_id"
    t.integer "possible_position_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "application_staffs", force: :cascade do |t|
    t.string "surname"
    t.string "name"
    t.string "patronymic"
    t.date "birth_date"
    t.integer "age"
    t.string "passport"
    t.string "birth_place"
    t.string "passport_issued_by"
    t.date "date_issue"
    t.date "validate_period"
    t.string "comment"
    t.string "tensure"
    t.string "qualification"
    t.date "treatment_date"
    t.date "interview_date"
    t.string "phone1"
    t.string "phone2"
    t.boolean "patent"
    t.boolean "migration_card"
    t.boolean "registration"
    t.boolean "med_book"
    t.boolean "stop_sheet"
    t.string "bank_card"
    t.boolean "import_record"
    t.integer "sex"
    t.integer "staff_uniq"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "nationality_id"
    t.integer "emp_type_id"
    t.integer "status_treatment_id"
    t.integer "status_interview_id"
    t.integer "etnicity_id"
    t.integer "stop_sheet_status_id"
    t.string "avatar"
    t.integer "residence_document_id"
    t.integer "manager_id"
    t.integer "desired_region_id"
    t.integer "patent_region_id"
    t.index ["emp_type_id"], name: "index_application_staffs_on_emp_type_id"
    t.index ["etnicity_id"], name: "index_application_staffs_on_etnicity_id"
    t.index ["manager_id"], name: "index_application_staffs_on_manager_id"
    t.index ["nationality_id"], name: "index_application_staffs_on_nationality_id"
    t.index ["residence_document_id"], name: "index_application_staffs_on_residence_document_id"
    t.index ["status_interview_id"], name: "index_application_staffs_on_status_interview_id"
    t.index ["status_treatment_id"], name: "index_application_staffs_on_status_treatment_id"
    t.index ["stop_sheet_status_id"], name: "index_application_staffs_on_stop_sheet_status_id"
  end

  create_table "emp_types", force: :cascade do |t|
    t.string "display_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "etnicities", force: :cascade do |t|
    t.string "display_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hirers", force: :cascade do |t|
    t.string "display_name"
    t.string "inn"
    t.string "address"
    t.string "ceo"
    t.string "org_phone"
    t.string "comment"
    t.string "contact_person1"
    t.string "phone1"
    t.string "contact_person2"
    t.string "phone2"
    t.string "history"
    t.integer "sphere_id"
    t.index ["sphere_id"], name: "index_hirers_on_sphere_id"
  end

  create_table "managers", force: :cascade do |t|
    t.string "display_name"
    t.boolean "ouster"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "nationalities", force: :cascade do |t|
    t.string "display_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "positions", force: :cascade do |t|
    t.string "display_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "possible_positions", force: :cascade do |t|
    t.string "display_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "regions", force: :cascade do |t|
    t.string "display_name"
  end

  create_table "residence_documents", force: :cascade do |t|
    t.string "display_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "settlements", force: :cascade do |t|
    t.datetime "date"
    t.datetime "shift"
    t.datetime "actual_arrival_time"
    t.datetime "lunch"
    t.datetime "time_care"
    t.datetime "actual_hours_worked"
    t.decimal "rate"
    t.decimal "amount"
    t.decimal "paid"
    t.decimal "debt"
    t.integer "payment_type"
    t.string "comments"
    t.boolean "going_to_work"
    t.integer "application_staff_id"
    t.integer "hirer_id"
    t.integer "position_id"
    t.index ["application_staff_id"], name: "index_settlements_on_application_staff_id"
    t.index ["hirer_id"], name: "index_settlements_on_hirer_id"
    t.index ["position_id"], name: "index_settlements_on_position_id"
  end

  create_table "spheres", force: :cascade do |t|
    t.string "display_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "status_interviews", force: :cascade do |t|
    t.string "display_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "status_treatments", force: :cascade do |t|
    t.string "display_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stop_sheet_statuses", force: :cascade do |t|
    t.string "display_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
