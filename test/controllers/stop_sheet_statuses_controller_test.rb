require 'test_helper'

class StopSheetStatusesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @stop_sheet_status = stop_sheet_statuses(:one)
  end

  test "should get index" do
    get stop_sheet_statuses_url
    assert_response :success
  end

  test "should get new" do
    get new_stop_sheet_status_url
    assert_response :success
  end

  test "should create stop_sheet_status" do
    assert_difference('StopSheetStatus.count') do
      post stop_sheet_statuses_url, params: { stop_sheet_status: {  } }
    end

    assert_redirected_to stop_sheet_status_url(StopSheetStatus.last)
  end

  test "should show stop_sheet_status" do
    get stop_sheet_status_url(@stop_sheet_status)
    assert_response :success
  end

  test "should get edit" do
    get edit_stop_sheet_status_url(@stop_sheet_status)
    assert_response :success
  end

  test "should update stop_sheet_status" do
    patch stop_sheet_status_url(@stop_sheet_status), params: { stop_sheet_status: {  } }
    assert_redirected_to stop_sheet_status_url(@stop_sheet_status)
  end

  test "should destroy stop_sheet_status" do
    assert_difference('StopSheetStatus.count', -1) do
      delete stop_sheet_status_url(@stop_sheet_status)
    end

    assert_redirected_to stop_sheet_statuses_url
  end
end
