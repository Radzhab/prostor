require 'test_helper'

class PossiblePositionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @possible_position = possible_positions(:one)
  end

  test "should get index" do
    get possible_positions_url
    assert_response :success
  end

  test "should get new" do
    get new_possible_position_url
    assert_response :success
  end

  test "should create possible_position" do
    assert_difference('PossiblePosition.count') do
      post possible_positions_url, params: { possible_position: {  } }
    end

    assert_redirected_to possible_position_url(PossiblePosition.last)
  end

  test "should show possible_position" do
    get possible_position_url(@possible_position)
    assert_response :success
  end

  test "should get edit" do
    get edit_possible_position_url(@possible_position)
    assert_response :success
  end

  test "should update possible_position" do
    patch possible_position_url(@possible_position), params: { possible_position: {  } }
    assert_redirected_to possible_position_url(@possible_position)
  end

  test "should destroy possible_position" do
    assert_difference('PossiblePosition.count', -1) do
      delete possible_position_url(@possible_position)
    end

    assert_redirected_to possible_positions_url
  end
end
