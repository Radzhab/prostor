require 'test_helper'

class ApplicationStaffsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @application_staff = application_staffs(:one)
  end

  test "should get index" do
    get application_staffs_url
    assert_response :success
  end

  test "should get new" do
    get new_application_staff_url
    assert_response :success
  end

  test "should create application_staff" do
    assert_difference('ApplicationStaff.count') do
      post application_staffs_url, params: { application_staff: {  } }
    end

    assert_redirected_to application_staff_url(ApplicationStaff.last)
  end

  test "should show application_staff" do
    get application_staff_url(@application_staff)
    assert_response :success
  end

  test "should get edit" do
    get edit_application_staff_url(@application_staff)
    assert_response :success
  end

  test "should update application_staff" do
    patch application_staff_url(@application_staff), params: { application_staff: {  } }
    assert_redirected_to application_staff_url(@application_staff)
  end

  test "should destroy application_staff" do
    assert_difference('ApplicationStaff.count', -1) do
      delete application_staff_url(@application_staff)
    end

    assert_redirected_to application_staffs_url
  end
end
